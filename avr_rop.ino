int LED_PIN = 13;
char tmp[0x100];

__attribute__((optimize("O0")))
void win() {
  pinMode(LED_PIN, HIGH);
  while(1) {
    digitalWrite(LED_PIN, HIGH);
    delay(100);
    digitalWrite(LED_PIN, LOW);
    delay(100);
  }
}

void setup() {
  Serial.begin(9600);
}

__attribute__((optimize("O0")))
void loop() {
  char buf[0x10] = {0};
  if(Serial.available() > 0){
    int ret = Serial.readBytes(tmp, 0x100);
    Serial.println("OK");  
    memcpy(buf, tmp, ret);
  }
}